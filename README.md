# uGT Test Crate

## Active

These names are reachable.

Boards:

* amc-s1e04-07-01.cms
* amc-s1e04-07-02.cms
* amc-s1e04-07-03.cms
* amc-s1e04-07-04.cms
* amc-s1e04-07-05.cms
* amc-s1e04-07-06.cms

AMC13s:

* amc-s1e04-07-13-t1.cms
* amc-s1e04-07-13-t2.cms

## Not reachable

Boards:

* amc-s1e04-07-07.cms
* amc-s1e04-07-08.cms
* amc-s1e04-07-09.cms
* amc-s1e04-07-10.cms
* amc-s1e04-07-11.cms
* amc-s1e04-07-12.cms

CPUs:

* amcuc-s1e04-07-07.cms
* amcuc-s1e04-07-08.cms
* amcuc-s1e04-07-09.cms
* amcuc-s1e04-07-10.cms
* amcuc-s1e04-07-11.cms
* amcuc-s1e04-07-12.cms