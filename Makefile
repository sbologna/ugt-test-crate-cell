SHELL:=/bin/bash
.DEFAULT_GOAL := help

#
# main variables
#

PROJECT_NAME=ugt-spare-configs
VERSION ?= $(shell git describe --always)
ARCH=amd64
RPM_NAME = ${PROJECT_NAME}-${VERSION}.${ARCH}.rpm

#
# main targets
#

.PHONY: rpm 
rpm: ${RPM_NAME} ## Builds RPMs

.PHONY: clean 
clean: ## Cleans up project
	rm -rf rpms rpmroot
#
# dependencies
#

${RPM_NAME}: ugt_spare_swatch_cell/*  ugt_spare_tcds_ici_cell/*  ugt_spare_tcds_pi_cell/*  ugt_spare_tstore/*
	
	rm -rf rpmroot
	mkdir -p rpms

	mkdir -p rpmroot/opt/xdaq/share/trigger/conf/ rpmroot/opt/xdaq/share/trigger/profile/ rpmroot/usr/lib/systemd/system
	
	cp ugt_spare_swatch_cell/ugt-spare-cell.{profile,configure,xhannel} rpmroot/opt/xdaq/share/trigger/profile/
	cp ugt_spare_tcds_ici_cell/tcds-gtup-spare-ici.{profile,xhannel} rpmroot/opt/xdaq/share/trigger/profile/
	cp ugt_spare_tcds_pi_cell/tcds-gtup-spare-pi.{profile,xhannel} rpmroot/opt/xdaq/share/trigger/profile/
	cp ugt_spare_tstore/ugt-spare-tstore.profile rpmroot/opt/xdaq/share/trigger/profile/
	
	cp ugt_spare_swatch_cell/ugt-spare-cell.env rpmroot/opt/xdaq/share/trigger/conf/
	cp ugt_spare_tcds_ici_cell/tcds-gtup-spare-ici.env rpmroot/opt/xdaq/share/trigger/conf/
	cp ugt_spare_tcds_pi_cell/tcds-gtup-spare-pi.env rpmroot/opt/xdaq/share/trigger/conf/
	cp ugt_spare_tstore/ugt-spare-tstore.env rpmroot/opt/xdaq/share/trigger/conf/

	cp ugt_spare_swatch_cell/trigger.ugt-spare-cell@.service rpmroot/usr/lib/systemd/system/
	cp ugt_spare_tcds_ici_cell/trigger.tcds-gtup-spare-ici@.service rpmroot/usr/lib/systemd/system/
	cp ugt_spare_tcds_pi_cell/trigger.tcds-gtup-spare-pi@.service rpmroot/usr/lib/systemd/system/
	cp ugt_spare_tstore/trigger.ugt-spare-tstore@.service rpmroot/usr/lib/systemd/system/

	cd rpmroot && fpm \
	-s dir \
	-t rpm \
	-n cactus-${PROJECT_NAME} \
	-v ${VERSION} \
	-a ${ARCH} \
	-m "<simone.bologna@cern.ch>" \
	--vendor CERN \
	--description "SWATCH, TCDS, and TStore configuration files of the uGT spare crate." \
	--url "https://gitlab.cern.ch/sbologna/ugt-test-crate-cell" \
	--provides cactus-${PROJECT_NAME} \
	.=/ && mv *.rpm ../rpms/

.PHONY: help
help: ## Displays this help
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
