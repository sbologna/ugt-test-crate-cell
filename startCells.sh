#!/usr/bin/env bash
export SETUP_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

export PID_DIR=/var/run
export LOCK_DIR=/var/lock
export CORE_DIR=/tmp
export XDAQ_ZONE=trigger
export XDAQ_EXTERN_LIBRARY_PATH=/opt/xdaq/lib
export XDAQ_USER=root
export XDAQ_ROOT=/opt/xdaq
export XDAQ_SETUP_ROOT=/opt/xdaq/share
export XDAQ_DOCUMENT_ROOT=/opt/xdaq/htdocs
export XDAQ_LOG=/var/log/trigger.tcds-gtup-ici.log


export XDAQ_CONFIG=${SETUP_DIR}/ugt_spare_swatch_cell/ugt-spare-cell.configure
export XDAQ_HOSTNAME=columbia.cern.ch
export LD_LIBRARY_PATH=/opt/xdaq/lib:/opt/cactus/lib:${LD_LIBRARY_PATH}

echo Running on ${XDAQ_HOSTNAME}

# echo STARTING TSTORE
/opt/xdaq/bin/xdaq.exe -h ${XDAQ_HOSTNAME} -p 7000 \
  -u file.append:/var/log/trigger.ugt-tstore.log \
  -e ${SETUP_DIR}/ugt_spare_tstore/ugt-spare-tstore.profile \
  -c ${XDAQ_CONFIG} \
  -z ${XDAQ_ZONE} &
echo STARTING SWATCH CELL
/opt/xdaq/bin/xdaq.exe -h ${XDAQ_HOSTNAME} -p 3333 \
  -u file.append:/var/log/trigger.ugt-cell.log \
  -e ${SETUP_DIR}/ugt_spare_swatch_cell/ugt-spare-cell.profile \
  -c ${XDAQ_CONFIG} \
  -z ${XDAQ_ZONE} &
echo STARTING ICI
/opt/xdaq/bin/xdaq.exe -h ${XDAQ_HOSTNAME} -p 4500 \
  -e ${SETUP_DIR}/ugt_spare_tcds_ici_cell/tcds-gtup-spare-ici.profile \
  -c ${XDAQ_CONFIG} \
  -z ${XDAQ_ZONE} 
echo STARTING PI
/opt/xdaq/bin/xdaq.exe -h ${XDAQ_HOSTNAME} -p 5500 \
  -u file.append:/var/log/trigger.tcds-gtup-pi.log \
  -e ${SETUP_DIR}/ugt_spare_tcds_pi_cell/tcds-gtup-spare-pi.profile \
  -c ${XDAQ_CONFIG} \
  -z ${XDAQ_ZONE}