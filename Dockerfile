FROM golang:1.14-alpine as demo-cell-in-docker

COPY hacks /hacks
WORKDIR /hacks/demo-cell-in-docker
RUN CGO_ENABLED=0 go build ./cmd/wrap.go

FROM gitlab-registry.cern.ch/cms-cactus/core/swatch/xdaq15-swatch13:tag-v1.3.0

COPY rpms/ /rpms/

RUN yum install -y /rpms/*.rpm && rm -rf /rpms

COPY --from=demo-cell-in-docker /hacks/demo-cell-in-docker/wrap /bin/xdaqwrapper